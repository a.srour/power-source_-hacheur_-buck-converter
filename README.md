**[Further Details are found in the PDF available in this repository as well as a proteus file to simulate the system.]**

**1-  Power Consumption:**

-Teensy board needs around 70mA at 5v for normal operation. It can withdraw 250mA in extreme cases while connected to several sensors.

-The BNO055 IMU sensor consumes around 12.5 mA at 3.3v.

-The led light consumes around 20 mA at 3.4 v.

-Finally, the transceivers consume around 70 mA at 5v.


**2-	DC/DC converter (Buck Converter):**

The buck converter called “Hacheur” is a type of switching regulator to convert the higher voltage into lower voltage. In our example we are converting 12 volts into 5 volts. Thus, we will be able to supply the teensy board 3.2 and the CAN transceiver with 5 volts. For the BNO055 sensor, it will be fed by 3.3v directly from the teensy or from 5v.

![](Pictures/Fig.1.png)

In the basic circuit above we have:

- 	Inductor mainly used for smoothing the current from ripples.
- 	Electrolyte Capacitor is to smooth the voltage.
- 	The switch which can be any type of high power and high frequency switch such as MOSFET’s or IGBT’s or thyristor classic.
- 	The diode is called “Diode de roue libre” to prevent negative flow of current.

As in Figure 1:

Switch closed: v_output= v_input (T_on)

Switch open: v_output= 0 (T_off)

Mean voltage: v_moyenne= αV

**3-	Design Phase:**

After choosing the battery and deciding on the Switching mode buck DC/DC. We now need to realize the circuit according to the available materials in the market. The main objective is to convert 12v to 5v able to withdraw up to 0.5 ampere at each ECU node in a very low cost and effective fashion. So, we will use MC34063 for that purpose which can be configured either for boost or buck mode.

![](Pictures/Fig.3.png)


Important Features of the IC:

1-	Current limit circuit.

2-	Controlled duty cycle oscillator.

3-	Accept 3V till 40 V.

4-	It can reach operations about 100 KHz.

5-	Adjustable output and very low standby current.



**3-	Simulation on Proteus:**

You can refer to the proteus file in the repository for simulating the system.

![](Pictures/Fig.4.png)


